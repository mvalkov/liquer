package liquer

import (
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
)

const configName = "config.toml"

func Load(vendor, app string, v interface{}) error {

	path, _ := getFile(vendor, app)
	if _, err := toml.DecodeFile(path, v); err != nil {
		return err
	}
	return nil
}

func Save(vendor string, app string, v interface{}) error {
	path, _ := getFile(vendor, app)
	file, _ := os.Create(path)
	defer file.Close()
	enc := toml.NewEncoder(file)
	enc.Encode(v)
	return nil
}

func getFile(vendor string, app string) (string, error) {
	path := filepath.Join(
		os.Getenv("HOME"),
		".config",
		vendor,
		app,
		configName,
	)
	if _, err := os.Stat(filepath.Dir(path)); err != nil {
		os.MkdirAll(filepath.Dir(path), os.ModePerm)
		os.Create(path)
	}

	return path, nil
}
