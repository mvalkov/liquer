package example

import "fmt"

func main() {

	Settings.Load()
	fmt.Println(Settings)
	Settings.Name = "Viola"
	Settings.Save()
	fmt.Println(Settings)
}
