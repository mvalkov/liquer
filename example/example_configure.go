package example

import (
	"github.com/meehalkoff/liquer"
)

const (
	vendor  = "vendor"
	appName = "app"
)

type settingsT struct {
	Name string
	Age  uint
	Kids []string
	Home string
}

var Settings = settingsT{}

func (s *settingsT) Load() error {
	return liquer.Load(vendor, appName, s)
}

// Save settings to file
func (s settingsT) Save() error {
	return liquer.Save(vendor, appName, s)
}
